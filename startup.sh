# startup.sh #

if hostname -f | grep "cloudera-master"; then
	./startup_master.sh
elif hostname -f | grep "cloudera-monitor"; then
	./startup_monitor.sh
elif hostname -f | grep "cloudera-slave"; then
	./startup_slave.sh
else
	echo "Please ensure this machine is the correct machine and is named appropriately. Failing."
fi
