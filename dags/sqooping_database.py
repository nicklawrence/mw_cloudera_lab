"""
Airflow DAG responsible for creating a Dataproc cluster, submitting Sqoop jobs from the Dataproc cluster against a
relational database, and moving that data to a specified filesystem.
"""

import datetime as dt

from airflow import models
from airflow.contrib.operators.dataproc_operator import DataprocClusterCreateOperator
from airflow.operators.bash_operator import BashOperator

# template based heavily from:
# https://medium.com/google-cloud/moving-data-with-apache-sqoop-in-google-cloud-dataproc-4056b8fa2600

###################################################################################################
# Database Settings
JDBC_DRIVER = ""  # 'mysql', 'postgresql', 'oracle:thin' etc...
DB_HOSTNAME = ""
DB_USERNAME = ""
DB_PW_FPATH = ""  # location of .txt file containing password, alternatively you can directly pass into Sqoop command
DB_PORT = ""
DB_NAME = ""

JDBC_FORMATS = {'postgresql' : f'jdbc:postgresql://localhost:{DB_PORT}/{DB_NAME}',
                'oracle:thin': f'jdbc:oracle:thin:@{DB_HOSTNAME}:{DB_PORT}:{DB_NAME}'
                }

###################################################################################################
# Dataproc Settings
CLUSTER_PROJECT_ID = ""
CLUSTER_NAME = ""
CLUSTER_ZONE = ""

MASTER_DISK_GIGS = 20
WORKER_DISK_GIGS = 20
N_WORKERS = 10
N_PREEMPTIBLE = 5

###################################################################################################
# Sqoop Configuration
JAR_BUCKET = ""
OUT_BUCKET = ""
# below are examples, depends on the details of job, requires the DB driver of the target database, manually downloaded
SQOOP_JOB_JARS = f'{JAR_BUCKET}/sqoop-1.4.7-hadoop260.jar,{JAR_BUCKET}/avro-tools-1.8.2.jar,{JAR_BUCKET}/postgresql-9.2-1002.jdbc4.jar,file:///usr/lib/hive/lib/hive-exec.jar'


class SqoopTableConfig:
    """
    Class responsible for consolidating configurations for one Sqoop ingestion.
    """

    file_format_map = {'avro'    : '--as-avrodatafile',
                       'parquet' : '--as-parquetfile',
                       'text'    : '--as-textfile',
                       'sequence': '--as-sequencefile'
                       }

    def __init__(self, schema_name, table_name, split_by_column=None, query=None):
        """
        :param schema_name: str value of schema table is in
        :param table_name: str value of table name

        :param split_by_column: column that Sqoop uses to parallelize, should be non-repeating, numeric primary key or
        timestamp

        :attribute query: sql string that determines logic for selecting from table. Should contain "$CONDITIONS"
        """
        self.schema_name = schema_name
        self.table_name = table_name

        self.split_by_column = split_by_column
        self.import_query = query or f"select * from {self.schema_name}.{self.table_name} where $CONDITIONS"

    def build_dir_path(self, out_bucket):
        n = dt.datetime.now()
        partitioning = f"{self.schema_name}/{self.table_name}/{n.year}/{n.month}/{n.day}"
        filename = f"{self.table_name}_{n.isoformat()}"
        return f"{out_bucket}/{partitioning}/{filename}"

    def build_import_command(self, out_bucket, file_format='avro'):

        # if no split by column exists, Sqoop is unable to parallelize ingestion, so we specify 1 worker instead
        if self.split_by_column:
            conditional_arg = f"--split-by {self.split_by_column}"
        else:
            conditional_arg = "--num-mappers=1"

        target_dir = self.build_dir_path(out_bucket)

        cmd = f"""
        gcloud dataproc jobs submit hadoop --cluster={CLUSTER_NAME} \
        --class=org.apache.sqoop.Sqoop \
        --jars={SQOOP_JOB_JARS} \
        -- import -Dmapreduce.job.user.classpath.first=true \
        --connect={JDBC_FORMATS[JDBC_DRIVER]} \
        --username={DB_USERNAME} \
        --password-file={DB_PW_FPATH} \
        --query '{self.import_query}' \
        {conditional_arg} \
        {self.file_format_map[file_format]} \
        -target-dir={target_dir}
        """

        return cmd


def to_mbs(n_gigs):
    """
    Converts gigabytes to megabytes
    """
    return n_gigs * 1024


def sqoop_table_to_gs(sqoop_config):
    """
    Returns BashOperator of Sqoop import command.
    """
    cmd = sqoop_config.build_import_command(OUT_BUCKET, 'avro')
    task_name = f"sqoop_{sqoop_config.schema_name}_{sqoop_config.table_name}"

    return BashOperator(task_id=task_name, bash_command=cmd)


def delete_dataproc_cluster(cluster_name):
    return BashOperator(task_id=f"delete_dataproc_cluster_{cluster_name}",
                        bash_command=f"gcloud dataproc clusters delete {cluster_name} --quiet --async")


sqoop_configs = [
    SqoopTableConfig(schema_name='sys', table_name='dba_tab_privs'),
    SqoopTableConfig(schema_name='sys', table_name='all_tables', query="SELECT owner, table_name FROM all_tables where $CONDITIONS"),
    SqoopTableConfig(schema_name='system', table_name='all_tables'),
    # SqoopConfig(schema_name='information_schema', table_name='columns')
    # etc...
]

DAG_TITLE = ""
DEFAULT_DAG_ARGS = {"start_date": dt.datetime(2018, 1, 1)}

with models.DAG(DAG_TITLE,
                schedule_interval=dt.timedelta(days=1),
                default_args=DEFAULT_DAG_ARGS) as dag:

    create_cluster = DataprocClusterCreateOperator(
        cluster_name=CLUSTER_NAME,
        task_id=f"Create-Cluster-{CLUSTER_NAME}",
        project_id=CLUSTER_PROJECT_ID,
        zone=CLUSTER_ZONE,
        image_version='1.2-debian9',

        # defines size of cluster
        master_disk_size=to_mbs(MASTER_DISK_GIGS),
        worker_disk_size=to_mbs(WORKER_DISK_GIGS),
        num_workers=N_WORKERS,
        num_preemptible_workers=N_PREEMPTIBLE,

        # misc. args defining cluster init behaviour
        service_account_scopes=[
            "https://www.googleapis.com/auth/devstorage.read_only",
            "https://www.googleapis.com/auth/logging.write",
            "https://www.googleapis.com/auth/monitoring.write",
            "https://www.googleapis.com/auth/pubsub",
            "https://www.googleapis.com/auth/service.management.readonly",
            "https://www.googleapis.com/auth/servicecontrol",
            "https://www.googleapis.com/auth/trace.append",
            "https://www.googleapis.com/auth/sqlservice.admin"
        ],
        metadata={
            "additional-cloud-sql-instances" : f"{DB_HOSTNAME}=tcp:{DB_PORT}",
            "enable-cloud-sql-hive-metastore": "false"
        },
        properties={
            "hive:hive.metastore.warehouse.dir": "gs://docker-lab-bucket/hive-warehouse"
        },
        init_actions_uris=[
            "gs://dataproc-initialization-actions/cloud-sql-proxy/cloud-sql-proxy.sh",
        ],
    )

    create_cluster >> [sqoop_table_to_gs(cfg) for cfg in sqoop_configs] >> delete_dataproc_cluster(CLUSTER_NAME)
