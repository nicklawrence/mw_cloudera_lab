# install_mysql_connector.sh #

echo "Starting MYSQL Connector Installation"

MYSQL_URL=https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.44.tar.gz

sudo wget $MYSQL_URL
sudo tar xvf "$(basename -- $MYSQL_URL)"
sudo mkdir -p /usr/share/java/
sudo cp mysql-connector-java-5.1.44/mysql-connector-java-5.1.44-bin.jar /usr/share/java/mysql-connector-java.jar

