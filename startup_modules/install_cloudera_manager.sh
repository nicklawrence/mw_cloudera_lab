# install_cloudera_manager.sh #

echo "Installing Cloudera Manager Service"

CLOUDERA_MANAGER_URL=https://archive.cloudera.com/cm5/redhat/7/x86_64/cm/cloudera-manager.repo
CLOUDERA_MANAGER_BASENAME="$(basename -- $CLOUDERA_MANAGER_URL)"

wget $CLOUDERA_MANAGER_URL
sudo cp $CLOUDERA_MANAGER_BASENAME /etc/yum.repos.d/

sudo yum -y install cloudera-manager-daemons cloudera-manager-server
echo "clouderacm" | sudo /usr/share/cmf/schema/scm_prepare_database.sh mysql cloudera_cm clouderacm
sudo service cloudera-scm-server start

echo "Finished Installating Cloudera Manager Service"
