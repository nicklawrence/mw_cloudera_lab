# install_java.sh #
# this file requires that you have already installed oracle home JDK8 from https://www.oracle.com/technetwork/java/javase/downloads/index.html #


JAVA_URL=http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz

JAVA_BASENAME="$(basename -- $JAVA_URL)"
JAVA_BASENAME_UTF="jdk1.8.0_131"
JAVA_DIR="/usr/java"

wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" $JAVA_URL
tar xvf $JAVA_BASENAME

sudo rm -r -f "/usr/java"
sudo mkdir $JAVA_DIR
sudo mv $JAVA_BASENAME_UTF $JAVA_DIR

export JAVA_HOME=$JAVA_DIR/$JAVA_BASENAME_UTF  ##  Add this to .bashrc as well
echo "export JAVA_HOME=$JAVA_DIR/$JAVA_BASENAME_UTF" >> ~/.bashrc
