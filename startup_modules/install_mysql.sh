# install_mysql.sh #

echo "Starting MYSQL Installation"

MYSQL_RPM="http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm"
DATABASES="cloudera_cm,cloudera_hive,cloudera_hue,cloudera_oozie"
PASSWORD="clouderacm"

# install mysql #
yum -y install wget
wget $MYSQL_RPM
rpm -ivh "$(basename -- $MYSQL_RPM)"
yum -y update
yum -y install mysql-server

# create_directories #
cp ./configs/mysql.conf /etc/my
mkdir -p /data/mysql
mkdir -p /data/mysql/lib/mysql
mkdir -p /data/mysql/log
mkdir -p /data/mysql/run/mysqld

RETURN_TO_DIR=$PWD
cd /data

chmod -R  755 mysql
chown -R mysql:mysql mysql

# start mysql service first
sudo service mysqld start
# trigger and automate answers of mysql installation #
# this also creates root user and sets pw #
/usr/bin/mysql_secure_installation <<EOF
$PASSWORD 
n
n
n
Y
Y
EOF

# create databases #
for db in ${DATABASES//,/ }
do
	echo "MYSQL: Creating database $db"
	mysql -uroot -e "CREATE DATABASE IF NOT EXISTS $db DEFAULT CHARACTER SET utf8;"
	mysql -uroot -e "GRANT ALL ON $db.* to 'clouderacm'@'localhost' IDENTIFIED BY '$PASSWORD';"
done

echo "Finished MYSQL Installation"
