# set_selinux.sh #

# disable SELINUX variable - if first time doing this, you need to restart the VM
sudo sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config
sudo reboot