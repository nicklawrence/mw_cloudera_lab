# startup_monitor.sh #

sudo ./startup_modules/upgrade_package_utilities.sh
sudo ./startup_modules/install_mysql.sh
sudo ./startup_modules/install_mysql_connector.sh
sudo ./startup_modules/install_java.sh

# only create monitor db on monitor host #
if grep "cloudera-monitor" /etc/hosts; then
	echo "MYSQL: Creating database cloudera_mntr"
	mysql -uroot -e "CREATE DATABASE IF NOT EXISTS cloudera_mntr DEFAULT CHARACTER SET utf8;"
	mysql -uroot -e "GRANT ALL ON cloudera_mntr.* to 'clouderacm'@'%';" #IDENTIFIED BY '<Password>';
fi

