# deploy zookeeper #

# CONFIGS #

NODE_ID   = $1
DIR       = /var/lib/zookeeper
CONF_PATH = $DIR/zoo.cfg
MYID_PATH = $DIR/myid

# ZOOKEEPER DEPLOY #

sudo apt-get install zookeeper-server

mkdir -p $DIR
chown -R zookeeper $DIR

sudo service zookeeper-server init
sudo service zookeeper-server start

echo $NODE_ID > $MYID_PATH
