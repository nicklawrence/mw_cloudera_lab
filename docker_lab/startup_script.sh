# MW Cloudera Startup Script #

CONTAINER_NAME="cloudera_lab"
DEPLOY_SCRIPTS="deploy_scripts"
MOUNTED_TEST_DATA="test_data"

# Start container, copy deploy scripts to container, then execute deploy scripts #

# install up-to-date Cloudera Docker image #
docker pull cloudera/quickstart:latest &&

# start Docker container, with mounted directory and specific open ports #
docker run --name=$CONTAINER_NAME \
--hostname=quickstart.cloudera \
--privileged=true \
-d -t -i \
-p 1-65535:1-65535/tcp \
-v /home/test_data:/mnt/test_data \
cloudera/quickstart \
/usr/bin/docker-quickstart &&

# start Cloudera Manager #
docker exec -it $CONTAINER_NAME /home/cloudera/cloudera-manager --express --force &&

docker cp $DEPLOY_SCRIPTS $CONTAINER_NAME:$DEPLOY_SCRIPTS
