# clean_docker.sh #
# run as root #

sudo docker stop "$(docker ps -a -q)" &&
sudo docker kill "$(docker ps -a -q)" &&
sudo docker rm "$(docker ps -a -q)" &&
sudo docker rmi "$(docker images -q)" 
